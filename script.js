window.onload = function() {
 
var lin_cfnt, log_cfnt;
var point_count = 400;

var controls = new function() {
	this.a = 01;
	this.b = 0.16;
	this.speed = 0.01;
	this.spiralsCount =1;
};

var gui = new dat.GUI();
gui.add(controls, 'a', 0, 10);
gui.add(controls, 'b', 0, 10);
gui.add(controls, 'speed', -1, 1);
gui.add(controls, 'spiralsCount', 1, 10);

var _width = $(document).width();
var _height = $(document).height();
/* To display anything, need a scene, a camera, and renderer */
var scene = new THREE.Scene();
scene.fog = new THREE.Fog(0xffffff, 1, 10000);
var camera = new THREE.PerspectiveCamera(
	90, //field of view
	_width / _height, 
	0.1, 
	2200 
);

camera.position.z = 800;
camera.position.y = -50;
camera.position.x = -80;

var renderer = new THREE.WebGLRenderer({
	antialias: true
});
renderer.setSize(_width, _height);
renderer.setClearColor(0xffffff);
document.body.appendChild(renderer.domElement);

geometry = new THREE.Geometry();
geometry2 = geometry.clone();
material = new THREE.LineBasicMaterial({
	color: 0x33cccc,
	linewidth: 5
});
// material = customMaterial;

var clear_geo = function() {
	geometry.vertices = [];

};

var render = function() {
 
	line.rotation.z -= controls.speed;

	


	spiral();
	

	$('div.debug__point-counter').text(geometry.vertices.length);

	geometry.verticesNeedUpdate = true;
	renderer.render(scene, camera);
};

var animate = function() {
	requestAnimationFrame(animate);
	render();
};

var spiral = function() {
	clear_geo();

	var lin_cfnt = controls.a;
	var log_cfnt = controls.b;
	var x_position, y_position, z_position;

	for (var i = 0.01; i <= point_count; i = i + 0.1) {

		x_position = lin_cfnt * Math.pow(Math.E, log_cfnt * i) * Math.cos(+i);
		y_position = lin_cfnt * Math.pow(Math.E, log_cfnt * i) * Math.sin(+i);
		z_position = 100 * i;
		geometry.vertices.push(new THREE.Vector3(x_position, y_position, z_position)); //x, y, z

	}
	
		
};

line = new THREE.Line(geometry, material);
line2 = new THREE.Line(geometry2, material);
scene.add(line, line2);


line.scale.x = line.scale.y = line.scale.z = 0.01;

animate();
	
	

	
	
};